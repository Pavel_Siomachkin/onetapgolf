﻿using System.Collections;
using System;
using UnityEngine;

namespace OneTapGolf
{
    public class Game : MonoBehaviour
    {
        public static Game instance;
        [HideInInspector] public Ball ball;

        private IGameScore gameScore;
        private IGameInstantiate gameInstantiate;
        private IGameInput gameInput;
        private IEnumerator coroutine;
        private int currentLevel;

        public event Action OnRestart;


        private void Awake()
        {
            instance = this;
            gameScore = GetComponent<IGameScore>();
            gameInstantiate = GetComponent<IGameInstantiate>();
            gameInput = GetComponent<IGameInput>();
        }

        private void Start()
        {
            currentLevel = 1;
            ball = gameInstantiate.GetBallObject();
            ball.OnGoal += Win;
            gameInput.OnDown += StartAim;
            gameInput.OnUp += StartFly;
        }

        private void StartAim()
        {
            ball.stateMashineController.SetAimState();
        }

        private void StartFly()
        {
            ball.stateMashineController.SetFlyState();
        }

        private void Win()
        {
            currentLevel++;

            float flagPositionX = gameInstantiate.GetFlagPosition().x ;
            gameScore.SetScore(flagPositionX);
            StartCoroutine(WaitAndRestart(2.0f));
        }

        private IEnumerator WaitAndRestart(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            ball.ResetBall();
            gameInstantiate.SetFlagPosition();
            OnRestart?.Invoke();
        }

        public void Restart()
        {
            currentLevel = 1;
            gameScore.ResetScore();
            ball.ResetBall();
            gameInstantiate.SetFlagPosition();
            OnRestart?.Invoke();
        }

        public int GetCurrentLevel()
        {
            return currentLevel;
        }
    }
}