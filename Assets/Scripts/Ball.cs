﻿using System;
using UnityEngine;


namespace OneTapGolf
{

    public class Ball : MonoBehaviour
    {

        public States currentState;
        public Vector2 Speed { get; set; }

        public event Action OnGoal;
        public event Action OnGameOver;
        public IBallStateMashineController stateMashineController;

        [SerializeField] private BallMovement ballMovement;
        [SerializeField] private Vector2 startSpeed;
        [SerializeField] private Rigidbody2D rigidBody;
        [SerializeField] private Animator stateMashine;

        private IBallTrajectory trajectory;
        private Vector3 startPosition = new Vector3(3, 2.3f, 0);


        private void Awake()
        {
            trajectory = GetComponent<IBallTrajectory>();
            stateMashineController = GetComponent<IBallStateMashineController>();
        }

        private void Start()
        {
            trajectory.OnLeftScreen += stateMashineController.SetFlyState;
            Speed = startSpeed;
        }       
      
        public void ShowTrajectory()
        {
            trajectory.Show(transform.position, Speed);
        }

        private void HideTrajectory()
        {
            trajectory.Hide();
        }

        public void Run()
        {
            HideTrajectory();
            ballMovement.Run(Speed);
        }

        public void ResetBall()
        {
            Speed = startSpeed;
            transform.position = startPosition;
            rigidBody.Sleep();
            stateMashineController.SetStartState();
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.CompareTag("Edge") && currentState == States.Fly && currentState != States.GameOver)
            {
                currentState = States.GameOver;
                stateMashineController.SetGameOverState();
                OnGameOver?.Invoke();

            }

        }

        private void OnTriggerEnter2D(Collider2D collision)
        {           
            if (collision.gameObject.CompareTag("Edge") && currentState == States.Fly)
            {
                currentState = States.Win;
                stateMashineController.SetWinState();
                OnGoal?.Invoke();
              
            }
        }  
    }
}