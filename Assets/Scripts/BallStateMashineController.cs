﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace OneTapGolf
{
    public enum States
    {
        Start,
        Aim,
        Fly,
        GameOver,
        Win
    }

    public class BallStateMashineController : MonoBehaviour, IBallStateMashineController
    {
        [SerializeField] private Animator stateMashine;

        public void SetFlyState()
        {
            stateMashine.SetTrigger("startFly");
        }

        public void SetAimState()
        {
            stateMashine.SetTrigger("startAim");
        }

        public void SetStartState()
        {
            stateMashine.SetTrigger("restart");
        }

        public void SetWinState()
        {
            stateMashine.SetTrigger("goal");
        }

        public void SetGameOverState()
        {
            stateMashine.SetTrigger("fail");
        }
    }
}
