﻿using UnityEngine;

namespace OneTapGolf
{
    public class CameraSize : MonoBehaviour
    {
        public static float width;
        Camera cameraMain;

        void Start()
        {
            cameraMain = Camera.main;
            width = 2f * cameraMain.orthographicSize * cameraMain.aspect;
            cameraMain.transform.position = new Vector3(width / 2, cameraMain.transform.position.y, cameraMain.transform.position.z);
        }
    }
}