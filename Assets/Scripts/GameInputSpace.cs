﻿using System;
using UnityEngine;

namespace OneTapGolf
{
    public class GameInputSpace : MonoBehaviour, IGameInput
    {
        public event Action OnDown;
        public event Action OnUp;

        private void Update()
        {      
                if (Input.GetKeyDown(KeyCode.Space) && Game.instance.ball.currentState == States.Start)
                {
                    OnDown?.Invoke();
                }
                if (Input.GetKeyUp(KeyCode.Space) && Game.instance.ball.currentState == States.Aim)
                {
                    OnUp?.Invoke();
                }
        }

    }
}
