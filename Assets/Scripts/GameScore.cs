﻿using System;
using UnityEngine;

namespace OneTapGolf
{
    public class GameScore : MonoBehaviour, IGameScore
    {

        public event Action<int, int> OnIncreasePoints;

        private int score, maxScore, points = 1;


        private void Start()
        {
            maxScore = PlayerPrefs.GetInt("score");
        }

        public void SetScore(float flagPosition)
        {
           
            score += points;

            if (score > maxScore)
            {
                PlayerPrefs.SetInt("score", score);
                maxScore = score;
            }

            OnIncreasePoints?.Invoke(score, points);
        }

        public void ResetScore()
        {
            score = 0;
        }

    }
}
