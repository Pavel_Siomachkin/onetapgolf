﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OneTapGolf
{
    public class GameInstantiate : MonoBehaviour, IGameInstantiate
    {

        [SerializeField] private GameObject ballPrefab, flagPrefab, scoreIncText;
        [SerializeField] private Vector3 startBallPosition;
        [SerializeField] private Transform golfObjets;

        private Ball ball;
        private GameObject flag;
        

        private void Start()
        {
            InstantiateFlag();

            InstantiateBall();
        }

        private void InstantiateFlag()
        {
            flag = Instantiate(flagPrefab);
            SetFlagPosition();
            flag.transform.parent = golfObjets;
        }

        private void InstantiateBall()
        {
            GameObject ballObject = Instantiate(ballPrefab, startBallPosition, Quaternion.identity);
            ballObject.transform.parent = golfObjets;
            ball = ballObject.GetComponent<Ball>();
        }

        public Ball GetBallObject()
        {
            return ball;
        }

        public void SetFlagPosition()
        {
            flag.transform.position = new Vector3(Random.Range(5f, CameraSize.width - 1f), 3.282f, 0);
            scoreIncText.transform.position = new Vector3(flag.transform.position.x, scoreIncText.transform.position.y, scoreIncText.transform.position.z);
        }

        public Vector3 GetFlagPosition()
        {
            return flag.transform.position;
        }

    }
}
