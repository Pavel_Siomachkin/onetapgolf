﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace OneTapGolf
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private Text levelText, scoreIncText;
        [SerializeField] private TMP_Text scoreText, scoreTextGameOver, bestText;
        [SerializeField] private Animator levelTextAnimator;
        [SerializeField] private Game game;
        [SerializeField] private GameScore gameScore;
        [SerializeField] private Animator scoreAnimator, learnAnimator;

        private Ball ball;
        private int currentLevel;
        private int maxScore;
        private int isFirstPlay;

        private void Start()
        {
            
            maxScore = PlayerPrefs.GetInt("score");
            bestText.text = maxScore.ToString();

            currentLevel = 1;
            ball = game.ball;

            ball.OnGoal += Win;
            ball.OnGameOver += GameOver;
            game.OnRestart += ShowLevelText;
            gameScore.OnIncreasePoints += IncreasePoints;

            levelTextAnimator.SetTrigger("isPlay");

            isFirstPlay = PlayerPrefs.GetInt("isFirstPlay", 0);
            if (isFirstPlay == 0)
            {
                PlayerPrefs.SetInt("isFirstPlay", 1);
                learnAnimator.SetTrigger("isStart");
            }
        }

        private void Win()
        {
            currentLevel++;
            levelText.text = "Level " + currentLevel;
        }

        private void GameOver()
        {
            gameOverPanel.SetActive(true);
        }

        private void IncreasePoints(int score, int points)
        {
            scoreText.text = score.ToString();
            scoreTextGameOver.text = score.ToString();
            if (score > maxScore)
            {
                bestText.text = score.ToString();
                maxScore = score;
            }
            else
            {
                bestText.text = maxScore.ToString();
            }
            scoreIncText.text = "+" + points;
            scoreAnimator.SetTrigger("isStart");
        }

        private void ShowLevelText()
        {
            levelTextAnimator.SetTrigger("isPlay");
        }

        public void Restart()
        {
            gameOverPanel.SetActive(false);
            currentLevel = 1;
            scoreText.text = "0";
            scoreTextGameOver.text = "0";
            levelText.text = "Level " + currentLevel;
        }
    }
}