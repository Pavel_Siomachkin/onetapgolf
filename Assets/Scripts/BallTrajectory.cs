﻿using System;
using UnityEngine;

namespace OneTapGolf
{
    public class BallTrajectory : MonoBehaviour, IBallTrajectory
    {

        [SerializeField] LineRenderer lineRendererComponent;
        public event Action OnLeftScreen;

        public void Show(Vector3 origin, Vector3 speed)
        {
            Vector3[] points = new Vector3[100];
            lineRendererComponent.positionCount = points.Length;
            lineRendererComponent.enabled = true;

            for (int i = 0; i < points.Length; i++)
            {
                float time = i * 0.1f;

                points[i] = origin + speed * time + Physics.gravity * time * time / 1.98f;

                if (points[i].y < 2)
                {
                    lineRendererComponent.positionCount = i + 1;
                    break;
                }
            }

            lineRendererComponent.SetPositions(points);

            if (points[lineRendererComponent.positionCount - 1].x > CameraSize.width)
            {
                OnLeftScreen?.Invoke();
            }
        }

        public void Hide()
        {
            lineRendererComponent.enabled = false;
        }
    }
}
