﻿using System;
using UnityEngine;


namespace OneTapGolf
{
    public class GameInputStartButton : MonoBehaviour, IGameInput
    {

        public event Action OnDown;
        public event Action OnUp;

        private int clickCount = 0;
    
        public void ClickDown()
        {
            if(Game.instance.ball.currentState == States.Start)
            {
                OnDown?.Invoke();
            }
                
        }

        public void ClickUp()
        {
            if (Game.instance.ball.currentState == States.Aim)
            {
                OnUp?.Invoke();
            }
           

        }


    }

}
