﻿using UnityEngine;

public class BallMovement : MonoBehaviour
{

    [SerializeField] private Rigidbody2D rb;


    public void Run(Vector2 speed)
    {
        rb.AddForce(speed, ForceMode2D.Impulse);
    }
}
