﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace OneTapGolf
{
    public class Start : BallFSM
    {

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);
            ball.currentState = States.Start;
        }

    }
}
    
