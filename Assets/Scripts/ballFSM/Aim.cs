﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace OneTapGolf
{

    public class Aim : BallFSM
    {

        private int currentLevel;

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

            base.OnStateEnter(animator, stateInfo, layerIndex);
            currentLevel = Game.instance.GetCurrentLevel();
            ball.currentState = States.Aim;
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
           
            ball.Speed += new Vector2(Time.deltaTime * (1f + currentLevel * 0.25f), Time.deltaTime * (2.5f + currentLevel * 0.25f));
            ball.ShowTrajectory();
        }


    }

}
