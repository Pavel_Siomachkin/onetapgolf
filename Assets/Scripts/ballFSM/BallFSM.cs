﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OneTapGolf
{

    public class BallFSM : StateMachineBehaviour
    {

        protected Ball ball;

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            ball = animator.gameObject.GetComponent<Ball>();
        }
    }
}