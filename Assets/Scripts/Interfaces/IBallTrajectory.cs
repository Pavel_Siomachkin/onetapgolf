﻿using System;
using UnityEngine;

namespace OneTapGolf
{
    public interface IBallTrajectory
    {
        event Action OnLeftScreen;

        void Hide();
        void Show(Vector3 origin, Vector3 speed);
    }
}