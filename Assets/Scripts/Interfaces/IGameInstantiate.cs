﻿using UnityEngine;

namespace OneTapGolf
{
    public interface IGameInstantiate
    {
        Ball GetBallObject();
        Vector3 GetFlagPosition();
        void SetFlagPosition();
    }
}