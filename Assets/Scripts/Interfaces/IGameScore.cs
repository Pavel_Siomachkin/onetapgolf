﻿using System;

namespace OneTapGolf
{
    public interface IGameScore
    {

        event Action<int, int> OnIncreasePoints;

        void ResetScore();
        void SetScore(float flagPosition);
    }
}