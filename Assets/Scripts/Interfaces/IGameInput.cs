﻿using System;

namespace OneTapGolf
{
    public interface IGameInput
    {
        event Action OnDown;
        event Action OnUp;
    }
}