﻿namespace OneTapGolf
{
    public interface IBallStateMashineController
    {
        void SetAimState();
        void SetFlyState();
        void SetGameOverState();
        void SetStartState();
        void SetWinState();
    }
}